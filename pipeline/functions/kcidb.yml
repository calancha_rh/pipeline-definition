---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.kcidb-function-definitions: |
    # Export KCIDB pipeline functions

    # Create checkout ID which is reproducible for Brew/Koji/COPR production runs
    # while unique per run for everything else.
    #
    # Args:
    #   $1: Link to original pipeline if checkout ID from original pipeline should be rebuilt,
    #       "" if a new ID should be created
    function create_checkout_id {
      local origin_prefix="redhat:"
      local original_pipeline="${1}"

      if is_production || [[ -n "${original_pipeline}" ]] ; then
        if [ -n "${copr_build:-}" ] ; then
          echo "${origin_prefix}copr-${copr_build}"
          return
        fi

        if [ -n "${brew_task_id:-}" ] ; then
          # Retrieve the correct identifier of Brew/Koji from the last path component of the web URL
          echo "${origin_prefix}${web_url##*/}-${brew_task_id}"
          return
        fi
      fi

      if [[ -n "${original_pipeline}" ]] ; then
        # Retrieve the ID of the original pipeline
        echo "${origin_prefix}${original_pipeline##*/}"
      else
        # Default to pipeline ID for pipelines other than production buildsystem runs
        echo "${origin_prefix}${CI_PIPELINE_ID}"
      fi
    }

    # Get link for a specific KCIDB output file
    # Args:
    #   $1: Type of the object, checkout or build
    #   $2: "name" attribute of the output file to retrieve URL for
    function get_output_file_link {
      output_files=$(kcidb_"${1}" get --json output_files)
      jq -r --arg name "${2}" '.[] | select(.name==$name) | .url' <<< "${output_files}"
    }

    # Save kcidb data to KCIDB_JOB_PATH
    # Args:
    #   $@: additional args to pass to cki.kcidb.parser
    function kcidb {
      "${VENV_PY3}" -m cki.kcidb.parser "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" "$@"
    }

    function kcidb_checkout {
      kcidb checkout "${KCIDB_CHECKOUT_ID}" "$@"
    }

    function kcidb_build {
      kcidb build "${KCIDB_BUILD_ID}" "$@"
    }

    # Save buildsystem data to KCIDB
    # Args:
    #   $1: Type of the object, checkout or build
    function set_buildsystem_data {
      # We can currently only save Brew/Koji data, not COPR
      if [ -n "${brew_task_id:-}" ] ; then
        buildsystem_task_url="${web_url}/taskinfo?taskID=${brew_task_id}"
        "kcidb_${1}" append-dict misc/provenance function="executor" \
                     url="${buildsystem_task_url}" service_name="buildsystem"
      fi
    }

    # Save all checkout information we have at the start
    function kcidb_set_base_checkout_data {
      kcidb_checkout create
      kcidb_checkout set-bool valid false

      tree_name="${name}"
      if is_true "${coverage}" ; then
        tree_name="${tree_name}-coverage"
      fi
      kcidb_checkout set tree_name "${tree_name}"

      kcidb_checkout append-dict misc/provenance function="coordinator" url="${CI_PIPELINE_URL}" service_name="gitlab"
      set_buildsystem_data checkout
      kcidb_checkout set-bool misc/retrigger "$(get_retrigger)"
      kcidb_checkout set-bool misc/scratch "${scratch}"
      kcidb_checkout set misc/kernel_type "${kernel_type}"
      kcidb_checkout set misc/public "${public}"
      kcidb_checkout set-time start_time now
      kcidb_checkout set misc/source_package_name "${source_package_name}"

      [ -n "${submitter}" ] && kcidb_checkout set misc/submitter "${submitter}"
      [ -n "${mr_url:-}" ] && kcidb_checkout set misc/mr/url "${mr_url}"
      [ -n "${mr_id:-}" ] && kcidb_checkout set-int misc/mr/id "${mr_id}"

      [ -n "${report_rules}" ] && kcidb_checkout set misc/report_rules "${report_rules}"
      [ -n "${message_id:-}" ] && kcidb_checkout set message_id "${message_id}"
      kcidb_checkout set-bool misc/send_ready_for_test_pre "${send_ready_for_test_pre}"
      kcidb_checkout set-bool misc/send_ready_for_test_post "${send_ready_for_test_post}"
      kcidb_checkout set-json contacts "${checkout_contacts}"

      [ -n "${git_url:-}" ] && kcidb_checkout set git_repository_url "${git_url}"
      [ -n "${branch:-}" ] && kcidb_checkout set git_repository_branch "${branch}"

      if [ -n "${patch_urls:-}" ]; then
        patchset_files=$(jq -R \
          'split(" ") | map({"url": ., "name": rtrimstr("/") | split("/") | last})' <<< "${patch_urls}"
        )
        kcidb_checkout set-json patchset_files "${patchset_files}"
        patchset_hash="$(${VENV_PY3} -c \
          "import os; from cki.kcidb.utils import patch_list_hash; print(patch_list_hash(os.environ['patch_urls'].split()))"
        )"
      else
        # patchset_hash has to be set even if there were no patches!
        patchset_hash=""
      fi
      kcidb_checkout set patchset_hash "${patchset_hash}"
    }

    # Create checkout and build entries for prebuilt kernels (e.g. Brew, part of tested compose)
    function save_kcidb_for_prebuilt_kernels {
      kcidb_set_base_checkout_data
      kcidb_checkout set-bool valid true  # Checkouts for ready builds are always valid

      kcidb_build create "${KCIDB_CHECKOUT_ID}"
      set_buildsystem_data build
      kcidb_build append-dict misc/provenance function="coordinator" url="${CI_JOB_URL}" service_name="gitlab"
      kcidb_build set-time start_time now
      kcidb_build set misc/package_name "${package_name}"

      kcidb_checkout set misc/kernel_version "${kernel_version}"
      set_kernel_architecture_debug
      if ! is_true "${skip_test}"; then
        kcidb_build set-bool misc/test_plan_missing true
      fi
    }

    # Create a KCIDB test entry for a selftest
    # Args:
    #   $1: subset name
    #   $2: retcode form the make command
    function kcidb_add_selftest {
      test_prefix="kselftest build"
      canonical_subtest_name="${1// /_}"
      test_id="${KCIDB_BUILD_ID}_selftest_${canonical_subtest_name}"

      if [ "${2}" = "0" ] ; then
        test_status="PASS"
      else
        test_status="FAIL"
      fi

      kcidb test "${test_id}" create "${KCIDB_BUILD_ID}"
      kcidb test "${test_id}" set path "${test_prefix// /_}.${canonical_subtest_name}"
      kcidb test "${test_id}" set comment "${test_prefix} - ${1}"
      kcidb test "${test_id}" set status "${test_status}"
      kcidb test "${test_id}" set-bool waived True
      kcidb test "${test_id}" append-dict output_files name="selftests_${canonical_subtest_name}.log" \
                              url="$(artifact_url "artifacts/selftests_${canonical_subtest_name}.log")"
      kcidb test "${test_id}" append-dict misc/provenance function="executor" url="${CI_JOB_URL}" service_name="gitlab"
    }

    # Set all tests in KCIDB_DUMPFILE_NAME as SKIP
    function kcidb_skip_all_tests {
      for test_id in $(jq -r '.tests[]?.id' "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"); do
        kcidb test "${test_id}" set status 'SKIP';
      done
    }

    # Set tests without result in KCIDB_DUMPFILE_NAME to SKIP
    function kcidb_skip_missing_tests {
      for test_id in $(jq -r '.tests[]? | select(.status == null) | .id' "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"); do
        cki_echo_error "${test_id}: Status is null, setting to SKIP";
        kcidb test "${test_id}" set status 'SKIP';
        kcidb test "${test_id}" set-bool misc/forced_skip_status true;
      done
    }

    # Set test defaults in test list
    function kcidb_set_test_defaults {
      jq --arg debug "${DEBUG_KERNEL}" \
         '.tests[]?.misc.targeted = false | .tests[]?.misc.debug = ($debug | test("[Tt]rue"))' \
         "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}" | sponge "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
    }
