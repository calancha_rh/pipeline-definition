#!/bin/bash
# Add any bash functions needed in tests to this file.

# Allow running the tests locally
if [[ ! -v CI_PROJECT_DIR ]]; then
    export CI_PROJECT_DIR=${PWD}
    export CI_PIPELINE_ID=42
    export CI_JOB_NAME=job-name
    export CI_JOB_ID=51
    export CKI_CURL_CONFIG_FILE=${CKI_CURL_CONFIG_FILE:-/dev/null}
fi

# unmodified from cki_pipeline.yml
export GIT_CACHE_DIR=${CI_PROJECT_DIR}/git-cache
export WORKDIR=${CI_PROJECT_DIR}/workdir
export ARTIFACTS_DIR="${CI_PROJECT_DIR}/artifacts"
export ARTIFACTS_TEMP_SUFFIX="-temp"
export AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
export KCIDB_DUMPFILE_NAME="kcidb_all.json"
export ARTIFACTS_META_PATH="artifacts-meta.json"
export MERGELOG_PATH="artifacts/merge.log"
export test_runner="beaker"
export kpet_high_cost="triggered"
export domains="available"
export extra_baseline_run_set_patterns=""
export skip_beaker="false"
# modified from cki_pipeline.yml
export MAX_TRIES=5
export MAX_WAIT=5
export artifacts_mode=s3
export ARCH=x86_64
export kpet_tree_name="fedora"
export KPET_ARCH=x86_64

function _failed_init {
    # keep track of number of failed tests
    export FAILED_FILE
    FAILED_FILE=$(mktemp)
    trap 'rm -rf "${FAILED_FILE}"' EXIT
    echo 0 > "${FAILED_FILE}"
}

function _failed_check {
    # fail the script if any tests failed.
    echo
    FAILED="$(cat "${FAILED_FILE}")"
    if (( FAILED > 0 )); then
        cki_echo_error "${FAILED} tests failed."
        exit 1
    fi
    cki_echo_heading "All tests passed."
}

function _check_equal {
    echo
    cki_echo_notify "$4?"
    echo "  Observed: $3 == $1"
    echo "  Expected: $3 == $2"
    if [[ $1 == "$2" ]]; then
        cki_echo_heading "  Result: PASS"
    else
        cki_echo_error "  Result: FAIL"
        FAILED="$(cat "${FAILED_FILE}")"
        echo "$((FAILED + 1))" > "${FAILED_FILE}"
    fi
}
